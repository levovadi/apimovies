//
//  Colors.swift
//  DemoMoviesApi
//
//  Created by Mariana Méndez on 29/12/20.
//

import Foundation
import UIKit
import Transition

let colorPrimary = UIColor(red: 63/255, green: 81/255, blue: 181/255, alpha: 1)

enum Catalog: Int {
    case dissolve
    case move
    case push
    case reveal
    //  More to come!
    case numberOfItems
}

extension Catalog {
    var title: String {
        return String(describing: self).capitalized
    }
    
    var transitionAnimation: TransitionAnimation {
        switch self {
        case .dissolve: return DissolveTransitionAnimation()
        case .move: return MoveTransitionAnimation(forNavigationTransitionsAtEdge: .right)
        case .push: return PushTransitionAnimation(forNavigationTransitionsAtEdge: .right)
        case .reveal: return RevealTransitionAnimation(forNavigationTransitionsAtEdge: .left)
        default: fatalError("Unsupported transition")
        }
    }
}

class FadePushAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toViewController = transitionContext.viewController(forKey: .to) else {
            return
        }
        transitionContext.containerView.addSubview(toViewController.view)
        toViewController.view.alpha = 0
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            toViewController.view.alpha = 1
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}


class FadePopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to)
            else {
                return
        }
        
        transitionContext.containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            fromViewController.view.alpha = 0
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}



