//
//  MovieCell.swift
//  DemoMoviesApi
//
//  Created by Mariana Méndez on 28/12/20.
//

import Foundation
import UIKit
import AlamofireImage
import Network

class MovieCell: UICollectionViewCell {
    
    static let id = "idMovieCell"
    
    var product: MovieStruct? {
        didSet {
            guard let unwrappedProduct = product else {return}
            imageMovie.image = UIImage(named: unwrappedProduct.imageName)
            titleMovieLabel.text = unwrappedProduct.titleMovie
        }
    }
    
    let imageMovie: UIImageView = {
        let imageProduct = UIImageView()
        imageProduct.translatesAutoresizingMaskIntoConstraints = false
        imageProduct.image = UIImage(systemName: "icloud.and.arrow.down")
        return imageProduct
    }()
    
    let titleMovieLabel: UILabel = {
        let nameLabel = UILabel(frame: CGRect.zero)
        nameLabel.text = "name of product"
        nameLabel.font = UIFont(name: "HelveticaNeue", size: 14)
        nameLabel.textColor = UIColor.black
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.backgroundColor = UIColor.blue.withAlphaComponent(0)
        nameLabel.textAlignment = .center
        return nameLabel
    }()
    
    func configure(for movie: Movie) {
        if DetectNetwork.shared.checkNetwork() {
            if movie.poster_path != nil {
                let url = URL(string: MovieAPI.baseImageURLString + movie.poster_path!)!
                titleMovieLabel.text = movie.title
                imageMovie.af_setImage(withURL: url, completion:  { (response) in
                    let dataImage = self.imageMovie.image?.pngData()
                    ManagerMoviesOffline.shared.updateOneMovie(title: movie.title ?? "noEncontrado", queryKey: "dataImage", newData: dataImage ?? Data())
                })
            }
        } else {
            titleMovieLabel.text = movie.title
            let getDataImage = ManagerMoviesOffline.shared.fetchOneMovie(title: movie.title ?? "")
            guard let getOnlyData = getDataImage.0 else { return }
            imageMovie.image = UIImage(data: getOnlyData)
            if ((imageMovie.image?.pngData()?.isEmpty) == nil) {
                imageMovie.image = UIImage(systemName: "icloud.and.arrow.down")
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageMovie)
        addSubview(titleMovieLabel)
        self.backgroundColor = .white
        setAutolayout()
        imageMovie.af_cancelImageRequest()
        imageMovie.image = nil
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            imageMovie.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            imageMovie.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            imageMovie.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            imageMovie.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.00),
            imageMovie.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.90)
            ])
        
        NSLayoutConstraint.activate([
            titleMovieLabel.topAnchor.constraint(equalTo: self.imageMovie.bottomAnchor, constant: 0),
            titleMovieLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            titleMovieLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.80),
            titleMovieLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.10)
            ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
}
