//
//  MovieModel.swift
//  DemoMoviesApi
//
//  Created by Mariana Méndez on 28/12/20.
//

import Foundation

struct MovieStruct {
    let titleMovie: String
    let imageName: String
}
