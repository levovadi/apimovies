//
//  LocalDataManager.swift
//  DemoMoviesApi
//
//  Created by Mariana Méndez on 29/12/20.
//

import Foundation
import CoreData

public class ManagerMoviesOffline {
    
    public static let shared = ManagerMoviesOffline()
    let identifier: String  = "mendezCorp.DemoMoviesApi"
    let model: String       = "DemoMoviesApi"
    
    lazy var persistentContainer: NSPersistentContainer = {

        let messageKitBundle = Bundle(identifier: self.identifier)
        let modelURL = messageKitBundle!.url(forResource: self.model, withExtension: "momd")!
        let managedObjectModel =  NSManagedObjectModel(contentsOf: modelURL)
        
        let container = NSPersistentContainer(name: self.model, managedObjectModel: managedObjectModel!)
        container.loadPersistentStores { (storeDescription, error) in
            
            if let err = error{
                fatalError("❌ Loading of store failed:\(err)")
            }
        }
        
        return container
    }()
    
    public func createOrSaveMovie(save movie: Movie,category: String,dataImage: String, data: Data) {
        
        // MARK: validate if movie exist
        let movieResult =  fetchOneMovie(title: movie.title ?? "")
        if movieResult.1 ?? false { return }
        let context = persistentContainer.viewContext
        let mov = NSEntityDescription.insertNewObject(forEntityName: "LocalMovie", into: context)
        mov.setValue(movie.title, forKey: "original_title")
        mov.setValue(dataImage, forKey: "base64Image")
        mov.setValue(movie.release_date, forKey: "releaseDate")
        mov.setValue(movie.overview, forKey: "overview")
        mov.setValue(data, forKey: "dataImage")
        mov.setValue(category, forKey: "category")
        
        do {
            try context.save()
            print("✅ movie saved succesfuly")
        } catch let error {
            print("❌ Failed to create Tx: \(error.localizedDescription)")
        }
    
    }
    
    public func fetcAllLocalMovies(searchByCategory: String) -> [MovieLocalWithImage] {
        var consultData: [MovieLocalWithImage] = []
        let context = persistentContainer.viewContext
        let fetchResult : NSFetchRequest<LocalMovie> = LocalMovie.fetchRequest()
        fetchResult.fetchLimit = 100
        fetchResult.predicate = NSPredicate(format: "category = %@", searchByCategory )
        do {
            let resultado = try context.fetch(fetchResult)
            if resultado.count == 0 { return [] }
            for res in resultado as [NSManagedObject] {
                let title =  res.value(forKey: "original_title") as? String ?? ""
                //let base64Image = res.value(forKey: "base64Image") as! String
                let releaseDate = res.value(forKey: "releaseDate") as? String ?? ""
                let category = res.value(forKey: "category") as? String ?? ""
                let overview = res.value(forKey: "overview") as? String ?? ""
                let dataImage = res.value(forKey: "dataImage") as? Data ?? Data()
                let movieToAdd = MovieLocalWithImage(original_title: title, overview: overview, poster_path: dataImage, release_date: releaseDate,category: category)
                consultData.append(movieToAdd)
            }
        } catch let error {
            print("❌ Failed to consult movie: \(error.localizedDescription)")
        }
        return consultData
    }
   
    public func updateOneMovie(title: String, queryKey: String, newData: Data) {
        let context = persistentContainer.viewContext
        let fetchResult : NSFetchRequest<LocalMovie> = LocalMovie.fetchRequest()
              fetchResult.fetchLimit = 100
              fetchResult.predicate = NSPredicate(format: "original_title = %@", title)
        do {
            let movieForUpdate = try context.fetch(fetchResult)
            if movieForUpdate.count < 1 { return }
            movieForUpdate[0].setValue(newData, forKey: queryKey)
            for item in movieForUpdate {
                print("✅ movi consult update \(item)")
            }
            do {
                try context.save()
            } catch let error {
                print("❌ Failed to update create movie: \(error.localizedDescription)")
            }
        } catch let error {
            print("❌ Failed to update catch consult movie: \(error.localizedDescription)")
        }
    }
    
    public func fetchOneMovie(title: String) -> (Data? ,Bool? ) {
        var movieExist: Bool? = nil
        var imageData: Data? = nil
        let context = persistentContainer.viewContext
        let fetchResult : NSFetchRequest<LocalMovie> = LocalMovie.fetchRequest()
        
        fetchResult.fetchLimit = 1
        fetchResult.predicate = NSPredicate(format: "original_title = %@", title)
        
        do {
            let resultado = try context.fetch(fetchResult)
            imageData =  resultado.first?.dataImage
            if resultado.count > 0 { movieExist = true } else { movieExist = false }
              } catch let error {
                  print("❌ Failed to consult movie: \(error.localizedDescription)")
                movieExist = false
              }
        return (imageData,movieExist)
    }
    
    public func deleteAllMovies() {
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalMovie")
        let borrar = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(borrar)
        } catch let error as NSError  {
            print(" dont find", error)
        }
    }
    
}
