

import Foundation
import ObjectMapper

struct MovieResponse: Mappable {
    var totalPages: Int?
    var totalResults:Int?
    var results: [Movie]?
    
    init?(map: Map){
    }
    
    mutating func mapping(map: Map) {
        totalPages <- map["total_pages"]
        totalResults <- map["total_results"]
        results <- map["results"]
    }
}
