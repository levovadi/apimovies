

import Foundation
import ObjectMapper

public struct Movie: Mappable {
    var title: String?
    var overview: String?
    var poster_path: String?
    var release_date: String?
    
    public init?(map: Map) {
    }
    
    mutating public func mapping(map: Map) {
        title <- map["original_title"]
        overview <- map["overview"]
        poster_path <- map["poster_path"]
        release_date <- map["release_date"]
    }
}

public enum categoryMovie: String {
    case rated
    case favorites
    case recommendations
}

public struct MovieLocalWithImage: Codable {
    var original_title: String?
    var overview: String?
    var poster_path: Data?
    var release_date: String?
    var category: String?
}
