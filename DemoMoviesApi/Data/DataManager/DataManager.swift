

import RxSwift
import ObjectMapper

class DataManager {
    
    func getMovies(query: String, page: String) -> Observable<MovieResponse?> {
        return MovieAPI.getMovies(query: query, page: page)
    }
    
    func getMockMovies() -> MovieResponse? {
        let url = Bundle.main.url(forResource: "movies", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            let json = try JSONSerialization.jsonObject(with: jsonData) as! [String: AnyObject]
            guard let movieResponse = Mapper<MovieResponse>().map(JSONObject: json) else {
                return nil
            }
            return movieResponse
        } catch {
            print(error)
            return nil
        }
    }
    
    func getPerCategoryMovies(queryCategory: categoryMovie) -> MovieResponse?  {

        let semaphore = DispatchSemaphore(value: 0)
        var movieResponse: MovieResponse? = nil
        
        let headers = ["authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxOTMzNjMxMzFkYTc5ZjYzZDBiNWM2NmIyMmMwMzA2NCIsInN1YiI6IjVmZTNiNzk2MGQ0MTdlMDAzZDRkZjAyZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.6T0dZSwKi88ywNw2bVG2kPt8aQIjjVWiwOOrQtGOg0k"]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.themoviedb.org/4/account/5fe3b7960d417e003d4df02e/movie/\(queryCategory)?api_key=193363131da79f63d0b5c66b22c03064")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
    
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error?.localizedDescription ?? NSError())
                movieResponse = nil
                semaphore.signal()
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data ?? Data()) as! [String: AnyObject]
                    print(json)
                    let movieResponseMap = Mapper<MovieResponse>().map(JSONObject: json)
                    movieResponse = movieResponseMap
                    semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    movieResponse = nil
                    semaphore.signal()
                }
                
            }
        })
        
        dataTask.resume()
        semaphore.wait()
        return movieResponse
        
    }
    
}
