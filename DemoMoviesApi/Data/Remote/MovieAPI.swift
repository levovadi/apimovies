

import RxSwift
import RxAlamofire
import ObjectMapper

struct MovieAPI {

    static let apiKey = "193363131da79f63d0b5c66b22c03064"
    static let baseURLString = "https://api.themoviedb.org/3/search/movie"
    static let baseImageURLString = "https://image.tmdb.org/t/p/w154"
    
    static let globalScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    
    static func getMovies(query: String, page: String) -> Observable<MovieResponse?> {
        
        return json(.get, baseURLString, parameters: ["query": query, "page": page, "api_key": apiKey])
            .do(onNext: { e in let _ = e })
            .map{ result in  toMapper(fromJSON: result) }
            .subscribeOn(globalScheduler)
    }
    
    private static func toMapper(fromJSON jsonResult: Any) -> MovieResponse? {
        guard let movieResponse = Mapper<MovieResponse>().map(JSONObject: jsonResult) else {
            return nil
        }
        return movieResponse
    }
    
}
