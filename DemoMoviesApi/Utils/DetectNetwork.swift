//
//  DetectNetwork.swift
//  DemoMoviesApi
//
//  Created by Mariana Méndez on 30/12/20.
//

import Foundation
import Network

public class DetectNetwork {
    
    public static let shared = DetectNetwork()
    
    public let monitor = NWPathMonitor()
    
    public func checkNetwork() -> Bool {
        let semaphore = DispatchSemaphore(value: 0)
        var isConected = false
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                isConected = true
                semaphore.signal()
            } else {
                isConected = false
                semaphore.signal()
            }
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        semaphore.wait()
        return isConected
    }
    
}

