
import Foundation
import UIKit

protocol SplashDelegate {
    func splashAnimationDidFinish()
}

class SplashVC: UIViewController {

    // MARK: Attributes
    let splashTime: Double = 5
    public static var delegate: SplashDelegate?
    let transitionDelegate: UIViewControllerTransitioningDelegate = TransitionDelegate()
    
    // MARK: Outlets
    @IBOutlet weak var loginContainerView: UIView!
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.transitioningDelegate = transitionDelegate
        self.navigationController?.navigationBar.isHidden = true
        var timer = Timer()
        timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: true, block: { _ in
            self.animationTrigger(finalShot: false)
            
        })
        Timer.scheduledTimer(withTimeInterval: self.splashTime, repeats: false, block: { _ in
            timer.invalidate()
            self.animationTrigger(finalShot: false)
            
        })
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + splashTime) {
            self.view.subviews.forEach({
                if $0.tag == 500 {
                    $0.removeFromSuperview()
                    
                }
            })
            let nextVC = HomeController()
            nextVC.modalPresentationStyle = .fullScreen
            nextVC.transitionDelegate = self.transitionDelegate
            let navigation = UINavigationController(rootViewController: nextVC)
            navigation.modalPresentationStyle = .fullScreen
            self.present(navigation, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Methods
    func animationTrigger(finalShot: Bool) {
        (0...(finalShot ? 15 : 4)).forEach({ i in
            let dimension = 20 + drand48() * 10
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: dimension, height: 30))
            imageView.image = randomImage(randomNumber: Int.random(in: 0..<10))
            imageView.tag = finalShot ? 400 : 500
            let animation = CAKeyframeAnimation(keyPath: "position")
            animation.path = customPath(frame: self.view.frame, finalShot: finalShot).cgPath
            animation.duration = 3 + drand48()*4
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            imageView.layer.add(animation, forKey: nil)
            
            self.view.addSubview(imageView)
            
        })
    }
    
    // MARK: return random image
    
    func randomImage(randomNumber: Int) -> UIImage {
        
        switch randomNumber {
        case 0:
            return #imageLiteral(resourceName: "nachos")
        case 1:
            return #imageLiteral(resourceName: "ticket")
        case 2:
            return #imageLiteral(resourceName: "minion")
        case 3:
            return #imageLiteral(resourceName: "soda-can")
        case 4:
            return #imageLiteral(resourceName: "ice-cream")
        default:
            return #imageLiteral(resourceName: "popcorn")
        }
        
    }

}

func customPath(frame: CGRect, finalShot: Bool) -> UIBezierPath {
    let path: UIBezierPath = UIBezierPath()
    let x1: Double = 0
    let y1: Double = Double(0.5*frame.height)
    let x2: Double = finalShot ? Double(frame.width)*0.5*(1 + drand48()) : Double(frame.width) + 50.0
    let y2: Double = y1*(1 + sin(4*Double.pi*drand48()))
    path.move(to: CGPoint(x: x1, y: y1))
    let endPoint = CGPoint(x: x2, y: y2)
    let rndmYShift = y1 + drand48() * 1.2*y1
    let cp1 = CGPoint(x: 0.1*x2, y: 0.5*y1 - 0.5*rndmYShift)
    let cp2 = CGPoint(x: 0.6*x2, y: 1.2*y1 + 0.5*rndmYShift)
    path.addCurve(to: endPoint, controlPoint1: cp1, controlPoint2: cp2)
    return path
    
}

class TransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePopAnimator()
    }
}
