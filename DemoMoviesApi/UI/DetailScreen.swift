
import Foundation
import UIKit

class DetailController: UIViewController {
    
    let imageMovie: UIImageView = {
        let imageProduct = UIImageView()
        imageProduct.translatesAutoresizingMaskIntoConstraints = false
        imageProduct.image = UIImage(systemName: "icloud.and.arrow.down")
        return imageProduct
    }()
    
    lazy var titleMovie: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        lb.font = UIFont(name: "Helvetica-Bold", size: 24)
        lb.textColor = .white
        return lb
    }()
    
    lazy var subtitleMovie: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        lb.font = UIFont(name: "Helvetica-Bold", size: 16)
        lb.textColor = .white
        return lb
    }()
    
    lazy var verticalStack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fillEqually
        stack.addArrangedSubview(titleMovie)
        stack.addArrangedSubview(subtitleMovie)
        return stack
    }()
    
    let card: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 0.5
        view.layer.cornerRadius = 15
        view.layer.borderColor = colorPrimary.cgColor
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        return view
    }()
    
    let overview: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.adjustsFontSizeToFitWidth = true
        lb.numberOfLines = 0
        lb.textColor = .white
        lb.font = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        return lb
    }()
    
    var lineblue: UIView = {
        let line = UIView()
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = UIColor(red: 54/255.0, green: 169/255.0, blue: 225/255.0, alpha: 1.0)
        return line
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavItems()
        addSubview()
        setAutolayout()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "cinema") ?? UIImage())
    }
    
    public func configureScreen(movie: Movie?) {
        guard let currentMovie = movie else { return }
        if DetectNetwork.shared.checkNetwork() {
            guard let path = currentMovie.poster_path else { return }
            guard let url = URL(string: MovieAPI.baseImageURLString + path) else { return }
        imageMovie.af_setImage(withURL: url, completion:  { (response) in
            self.titleMovie.text = currentMovie.title
            self.subtitleMovie.text = currentMovie.release_date
            self.overview.text = currentMovie.overview
        })
        } else {
            self.titleMovie.text = movie?.title
            self.subtitleMovie.text = movie?.release_date
            self.overview.text = movie?.overview
            let getDataImage = ManagerMoviesOffline.shared.fetchOneMovie(title: movie?.title ?? "")
            guard let getOnlyData = getDataImage.0 else { return }
            imageMovie.image = UIImage(data: getOnlyData)
            if ((imageMovie.image?.pngData()?.isEmpty) == nil) {
                imageMovie.image = UIImage(systemName: "icloud.and.arrow.down")
            }
        }
    }
    
    private func setNavItems() {
        let backIcon = UIImage(named: "whiteArrow")
        let backItem = UIBarButtonItem(image: backIcon?.withRenderingMode(.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backPressed))
        backItem.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = backItem
    }
    
    @objc func backPressed() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func addSubview() {
        self.view.addSubview(imageMovie)
        self.view.addSubview(card)
        self.card.addSubview(verticalStack)
        self.card.addSubview(lineblue)
        self.card.addSubview(overview)
    }
    
    private func setAutolayout() {
        NSLayoutConstraint.activate([
            self.imageMovie.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8.0),
            self.imageMovie.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.imageMovie.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.30),
            self.imageMovie.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.50),
        ])
        
        NSLayoutConstraint.activate([
            self.card.topAnchor.constraint(equalTo: self.imageMovie.bottomAnchor, constant: 8.0),
            self.card.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8.0),
            self.card.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8.0),
            self.card.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            self.verticalStack.topAnchor.constraint(equalTo: self.card.topAnchor, constant: 8.0),
            self.verticalStack.widthAnchor.constraint(equalTo: self.card.widthAnchor, multiplier: 0.98),
            self.verticalStack.centerXAnchor.constraint(equalTo: self.card.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            self.lineblue.topAnchor.constraint(equalTo: self.verticalStack.bottomAnchor, constant: 10),
            self.lineblue.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.90),
            self.lineblue.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.005),
            self.lineblue.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            self.overview.topAnchor.constraint(equalTo: self.lineblue.bottomAnchor, constant: 16),
            self.overview.leadingAnchor.constraint(equalTo: self.card.leadingAnchor, constant: 8),
            self.overview.trailingAnchor.constraint(equalTo: self.card.trailingAnchor, constant: -8),
            self.overview.centerXAnchor.constraint(equalTo: self.card.centerXAnchor),
        ])
        
    }
    
}
