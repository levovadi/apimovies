//
//  CustomAlert.swift
//  DemoMoviesApi
//
//  Created by Mariana Méndez on 29/12/20.
//

import Foundation
import UIKit

public class AlertManager {
    
    func showAlert(title: String, description: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
        alert.addAction(acceptAction)
        alert.addAction(cancelAction)
        return alert
    }
    
}
