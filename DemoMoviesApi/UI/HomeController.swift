

import UIKit
import RxSwift
import RxCocoa
import Alamofire
import Network
import Transition

var selectedIndex: Int?
class HomeController: UIViewController, InteractiveNavigationTransitionOperationDelegate {
    
    // MARK: Variables
    var transitionDelegate: UIViewControllerTransitioningDelegate!
    private let items = ["Favoritas","Recomendaciones","Calificadas"]
    private let datamanager = DataManager()
    private let disposeBag = DisposeBag()
    private let behaviorRelay = BehaviorRelay<String>(value: "Game")
    var localMovies: [Movie?] = []
    var transitionController: TransitionController?
    let panInteractionController = PanInteractionController(forNavigationTransitionsAtEdge: .right)
    // MARK: User interface elements
    
    lazy var segmentCategories: UISegmentedControl = {
        let segment = UISegmentedControl(items: items)
        segment.translatesAutoresizingMaskIntoConstraints = false
        segment.layer.borderWidth = 2.0
        segment.layer.borderColor = UIColor.darkGray.cgColor
        segment.selectedSegmentTintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        segment.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
           segment.setTitleTextAttributes(titleTextAttributes, for: .normal)
           segment.setTitleTextAttributes(titleTextAttributes, for: .selected)
        segment.addTarget(self, action: #selector(handleChangeCategory(_:)), for: .valueChanged)
        return segment
    }()
    
    lazy var cardSection: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 20
        view.layer.borderWidth = 1.5
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        return view
    }()
    
    lazy var searchTextfield: UITextField = {
        let textfield = UITextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.font = UIFont(name: "Helvetica-Bold", size: 15)
        textfield.placeholder = "Ingrese un titulo de pelicula"
        textfield.borderStyle = .roundedRect
        return textfield
    }()
    
    lazy var searchButton: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(toggleSearch), for: .touchUpInside)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.font = UIFont(name: "helvetica-bold", size: 15)
        button.setTitle("     Búscar     ", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.borderWidth = 0.5
        button.layer.cornerRadius = 10
        button.layer.borderColor = UIColor.darkGray.cgColor
        button.backgroundColor = .systemBlue
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.8
        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return button
    }()
    
    lazy var resultLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Helvetica-bold", size: 12)
        label.text = "resultados: 0"
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    lazy var verStack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.addArrangedSubview(searchTextfield)
        stack.addArrangedSubview(searchButton)
        stack.addArrangedSubview(resultLabel)
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .center
        stack.spacing = 4.0
        return stack
    }()

    let moviesCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        let listMoviesCatalog = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listMoviesCatalog.register(MovieCell.self, forCellWithReuseIdentifier: MovieCell.id)
        listMoviesCatalog.translatesAutoresizingMaskIntoConstraints = false
        listMoviesCatalog.backgroundColor = UIColor.black.withAlphaComponent(0)
        listMoviesCatalog.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        listMoviesCatalog.isScrollEnabled = true
        return listMoviesCatalog
    }()
    
    // MARK: Default methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.view.backgroundColor = .white
        addSubviews()
        setCollectionInUI()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "cinema") ?? UIImage())
        transitionController = TransitionController(forInteractiveTransitionsIn: navigationController!, transitionsSource: self, operationDelegate: self, interactionController: panInteractionController)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if DetectNetwork.shared.checkNetwork() {
            // MARK: functionality first time
            searchTextfield.rx.text
                .map{ $0!.count > 3 && $0!.count < 10 }
                .bind(to: searchButton.rx.isEnabled)
                .disposed(by: disposeBag)
            
            searchButton.rx.tap
                .bind(onNext: { [unowned self] in
                    self.loadMovies(title: self.searchTextfield.text ?? "")
                })
                .disposed(by: disposeBag)
            
            //Driver
            let results = behaviorRelay
                .flatMapLatest({ [unowned self] title -> Observable<MovieResponse?> in
                    self.datamanager.getMovies(query: title, page: "1")
                })
                .asDriver(onErrorJustReturn: datamanager.getMockMovies())
            
            //Update CollectionView
            results
                .map{ ($0?.results)! }
                .drive(moviesCollection.rx.items(cellIdentifier: MovieCell.id, cellType: MovieCell.self))
                { (row, movie, cell) in
                    cell.configure(for: movie)
                }.disposed(by: disposeBag)
           
            results
                .map{ ($0?.results)! }
                .drive { (movies) in
                    self.localMovies = movies
                } onCompleted: {
                    print("oncompleted")
                } onDisposed: {
                    print("onDisposed")
                }.disposed(by: disposeBag)

            
            //Update Label
            results
                .map{ "Total results: \(($0?.totalResults)!)" }
                .drive(resultLabel.rx.text)
                .disposed(by: disposeBag)
        }
    
    }
    
    // MARK: Custom methods
    
    public func performOperation(operation: UINavigationController.Operation, forInteractiveTransitionIn controller: UINavigationController, gestureRecognizer: UIGestureRecognizer) {
        switch operation {
        case .push:
            if selectedIndex == nil {
                if let selectedCell = self.moviesCollection.visibleCells.first(where: { cell in
                    let frame = view.convert(cell.frame, from: cell.superview)
                    return frame.contains(gestureRecognizer.location(in: view))
                }) {
                    selectedIndex = moviesCollection.indexPath(for: selectedCell)?.row
                }
            }
            
        case .pop:
            let _ = navigationController?.popViewController(animated: true)
        default: break
        }
        
    }
    
    @objc func dismissKeyboard() {
//        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
//        view.addGestureRecognizer(tap)
        self.searchTextfield.text = ""
        view.endEditing(true)
    }
    
    fileprivate func addSubviews() {
        self.view.addSubview(segmentCategories)
        self.view.addSubview(cardSection)
        self.cardSection.addSubview(verStack)
        self.view.addSubview(moviesCollection)
    }
    
    private func setCollectionInUI() {
        self.moviesCollection.delegate = self
        setAutolayout()
    }
    
    @objc func handleChangeCategory(_ sender: UISegmentedControl) {
        dismissKeyboard()
        switch sender.selectedSegmentIndex {
        case 0:
            if DetectNetwork.shared.checkNetwork() {
                self.title = ""
                guard let movies = self.datamanager.getPerCategoryMovies(queryCategory: .favorites) else { return }
                guard let resultsMovies = movies.results else { return }
                self.localMovies.removeAll()
                for fav in resultsMovies  {
                    self.localMovies.append(fav)
                    ManagerMoviesOffline.shared.createOrSaveMovie(save: fav, category: "favorites", dataImage: "dummy", data: Data())
                }
                moviesCollection.dataSource = self
                moviesCollection.reloadData()
            } else {
                let localData = ManagerMoviesOffline.shared.fetcAllLocalMovies(searchByCategory: "favorites")
                self.title = "Offline"
                self.localMovies.removeAll()
                for item in localData {
                    let jsonEncoder = JSONEncoder()
                    let jsonData = try! jsonEncoder.encode(item)
                    let json = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
                    let mov = Movie(JSONString: json)
                    self.localMovies.append(mov)
                }
                moviesCollection.dataSource = self
                moviesCollection.reloadData()
            }
        case 1:
            if DetectNetwork.shared.checkNetwork() {
            guard let movies = self.datamanager.getPerCategoryMovies(queryCategory: .recommendations) else { return }
            guard let resultsMovies = movies.results else { return }
            self.localMovies.removeAll()
            for recommend in resultsMovies  {
                self.localMovies.append(recommend)
                ManagerMoviesOffline.shared.createOrSaveMovie(save: recommend, category: "recommendations", dataImage: "dummy", data: Data())
            }
            moviesCollection.dataSource = self
            moviesCollection.reloadData()
            } else {
                let localData = ManagerMoviesOffline.shared.fetcAllLocalMovies(searchByCategory: "recommendations")
                self.title = "Offline"
                self.localMovies.removeAll()
                for item in localData {
                    let jsonEncoder = JSONEncoder()
                    let jsonData = try! jsonEncoder.encode(item)
                    let json = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
                    let mov = Movie(JSONString: json)
                    self.localMovies.append(mov)
                }
                moviesCollection.dataSource = self
                moviesCollection.reloadData()
            }
        case 2:
            if DetectNetwork.shared.checkNetwork() {
            guard let movies = self.datamanager.getPerCategoryMovies(queryCategory: .rated) else { return }
            guard let resultsMovies = movies.results else { return }
            self.localMovies.removeAll()
            for rated in resultsMovies  {
                self.localMovies.append(rated)
                ManagerMoviesOffline.shared.createOrSaveMovie(save: rated, category: "rated", dataImage: "test", data: Data())
            }
            moviesCollection.dataSource = self
            moviesCollection.reloadData()
            } else {
                let localData = ManagerMoviesOffline.shared.fetcAllLocalMovies(searchByCategory: "rated")
                self.title = "Offline"
                self.localMovies.removeAll()
                for item in localData {
                    let jsonEncoder = JSONEncoder()
                    let jsonData = try! jsonEncoder.encode(item)
                    let json = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
                    let mov = Movie(JSONString: json)
                    self.localMovies.append(mov)
                }
                moviesCollection.dataSource = self
                moviesCollection.reloadData()
            }
        default:
            self.view.backgroundColor = .red
        }
    }
    
     @objc func toggleSearch() {
        view.endEditing(true)
    }
    
    private func loadMovies(title: String) {
        behaviorRelay.accept(title)
    }
    
    // MARK: Aligment ui elements

    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            self.segmentCategories.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8.0),
            self.segmentCategories.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.98),
            self.segmentCategories.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05),
            self.segmentCategories.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            self.cardSection.topAnchor.constraint(equalTo: self.segmentCategories.safeAreaLayoutGuide.bottomAnchor, constant: 8.0),
            self.cardSection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.98),
            self.cardSection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.15),
            self.cardSection.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            verStack.topAnchor.constraint(equalTo: self.cardSection.topAnchor, constant: 8.0),
            verStack.leadingAnchor.constraint(equalTo: self.cardSection.leadingAnchor, constant: 8.0),
            verStack.trailingAnchor.constraint(equalTo: self.cardSection.trailingAnchor, constant: -8.0),
            verStack.bottomAnchor.constraint(equalTo: self.cardSection.bottomAnchor, constant: -8.0),
        ])
        
        NSLayoutConstraint.activate([
            self.moviesCollection.topAnchor.constraint(equalTo: self.cardSection.bottomAnchor, constant: 8),
            self.moviesCollection.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            self.moviesCollection.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            self.moviesCollection.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        ])
        
    }
    
}

// MARK: Collection methods

extension HomeController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.localMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  MovieCell.id, for: indexPath) as! MovieCell
        guard let movie = self.localMovies[indexPath.row] else { return UICollectionViewCell() }
        cell.configure(for: movie)
        return cell
    }
    
}

extension HomeController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        let currentMovie = localMovies[indexPath.row]
        let detail = DetailController()
        detail.configureScreen(movie: currentMovie)
        detail.modalTransitionStyle = .partialCurl
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wCell = view.frame.width / 2.45
        let hCell = view.frame.height * 0.30
        return CGSize(width: wCell, height: hCell)
    }
    
}


extension UIViewController: TransitionsSource {
    
    public func transitionFor(operationContext: TransitionOperationContext, interactionController: TransitionInteractionController?) -> Transition {
        guard let transitionType = Catalog(rawValue: 0) else { fatalError("No transition found") }
        return Transition(duration: 0.5, animation: transitionType.transitionAnimation)
    }
    
}


