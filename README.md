
---

## Demo Api Movies

Es una aplicacion demo consumiendo la api del sitio https://www.themoviedb.org/ en el cual se debe crear una cuenta para generar un api key y un token de acceso para poder consumir los servicios, la cual funciona con o sin internet.

---

## Interfaz

En cuanto al diseño de la intefaz se utilizo UIKit con clases programaticas es decir sin el uso de storyboards asi como autolayout.

# Pods utilizados

## core RxSwift
  1. pod 'RxSwift', '~> 5'
  2. pod 'RxCocoa', '~> 5'
  3. pod 'RxDataSources', '~> 4.0'
  
  El core rx es un patron observador que ayuda en algunas partes de la aplicacion como por ejemplo a mostrar los datos en la interfaz cuando estos son descargados.
  
## Community projects
  4. pod 'Alamofire', '~> 4.8' (se utilizo para las peticiones hacia la api)
  5. pod 'AlamofireImage', '~> 3.5' (ayuda al tratamiento de imagenes como asu descarga o manejar cache para una mejor experiencia al usuario)
  6. pod 'RxAlamofire', '~> 5.0.0' (complemento para notificar que se han descargado los datos )
  7. pod 'AlamofireObjectMapper', '~> 5.2' (para ayudar al mapeo del response de los servicios)
  8. pod 'Transition' (efecto de transición en navigationcontroller)
  
  
## Arquitectura utilizada

 se utilizo como base el patrón MVC ya que se dividio la app en tres capas por asi decirlo, pero tambien se utilizaron otros patrones como singleton u observer .

1.  Capa de datos locales en donde se encargara de extraer la información local .
2.  Capa remota/comunicaciones que se encargara de administrar las peticiones hacia la api .
3. Controlador que se encargara de la logica entre la vista y datos.

Tambien se utilizo algunas clases singleton para funciones como por ejemplo para detectar si hay conexión a internet.

## Respuestas
1. las capas de aplicacion son la capa de datos locales , otra para datos remotos y una que se encarga de la logica o el controlador.
2. el proposito o la finalidad es que cada modulo le corresponda una funcion en especifico 
3. cuando se tiene un codigo limpio es facil darle mateniiento debido a que tiene una estructura sólida en cuanto a funcionamiento o modulos.
4. permite separar los componentes sin tanta complejidad como otros patrones que tienen mas componentes o modulos.
